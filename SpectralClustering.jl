#--------------------------------------------------------------------------
#--------------------------------------------------------------------------
# MATLAB code Copyright @ Ehsan Elhamifar, 2012
# Adapted to Julia by Martin Rodriguez, 2019
#--------------------------------------------------------------------------

using LinearAlgebra
using Clustering
using Gadfly

"""
    SpectralClustering(CKSym,n,REPlic=100)

This function takes an adjacency matrix of a graph and computes the 
clustering of the nodes using the spectral clustering algorithm of 
Ng, Jordan and Weiss.
# groups: N-dimensional vector containing the memberships of the N points 
# to the n groups obtained by spectral clustering

...
# Arguments

- `CKSym`: NxN adjacency matrix
- `n`: number of groups for clustering
- `REPlic=100`: Run 100 replications of kmeans to find best result by default
...

# Example
'''
'''
"""
function SpectralClustering(CKSym,n,REPlic = 100)

N = size(CKSym,1)
MAXiter = 1000 # Maximum number of iterations for KMeans 

# Normalized spectral clustering according to Ng & Jordan & Weiss
# using Normalized Symmetric Laplacian L = I - D^{-1/2} W D^{-1/2}
DN = diagm(vec(1 ./ sqrt.(sum(CKSym, dims=1) .+ eps())))
LapN = Matrix{Float64}(I, N, N) - DN * CKSym * DN
uN, sN, vN = svd(LapN)
kerN = vN[:,N-n+1:N]
kerNS = reshape([], 0, n)

for i = 1:N
    kerNS = vcat(kerNS, (kerN[i,:] ./ norm(kerN[i,:] .+ eps()))')
end

kerNS = reshape(hcat(kerNS...), N, n)
kmeansResult = kmeans(kerNS',n,n_init=REPlic,maxiter=MAXiter)

groups = kmeansResult.assignments
return groups

end
