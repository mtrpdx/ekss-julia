using Hungarian

"""
    myBestMap(trueLabels,estLabels)

Uses the Hungarian (Kuhn-Munkres) algorithm for linear sum assignment and outputs 
the best mapping of the labels.

...
# Arguments
- `trueLabels`: 
- `estLabels`: 
...

# Example
'''
'''
"""
function myBestMap(trueLabels,estLabels)
trueLabelVals = unique(trueLabels)
kTrue = length(trueLabelVals)
estLabelVals = unique(estLabels)
kEst = length(estLabelVals)

cost_matrix = zeros(kEst,kTrue)
for ii = 1:kEst
    inds = findall(estLabels .== estLabelVals[ii])
    for jj = 1:kTrue
        cost_matrix[ii,jj] = length(findall(trueLabels[inds] .== trueLabelVals[jj]))
    end
end

# Use the Hungarian algorithm for linear sum assignment
hOut = hungarian(-cost_matrix)
rInd = hOut[1]
cInd = collect(Int, 1:1:kTrue)

outLabels = Inf*ones(length(estLabels))
for ii = 1:length(rInd)
    estLabelsEQ = estLabels .== estLabelVals[rInd[ii]]
    outLabels[estLabelsEQ] .= trueLabelVals[cInd[ii]]
end

outLabelVals = unique(outLabels)
if length(outLabelVals) < maximum(outLabels)
    lVal = 1
    for ii = 1:length(outLabelVals)
        outLabels[outLabels.==outLabelVals[ii]] = lVal
        lVal = lVal + 1
    end
end

return outLabels
end
