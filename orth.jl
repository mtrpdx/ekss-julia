"""
orth(M)

Compute an orthogonal basis for matrix `A`.

Returns a matrix whose columns are the orthogonal vectors that
constitute a basis for the range of A.
If the matrix is square/invertible, returns the `U` factor of
`svdfact(A)`, otherwise the first *r* columns of U, where *r*
is the rank of the matrix.
# Examples
```julia
julia> orth([1 8 12; 5 0 7])
2×2 Array{Float64,2}:
 -0.895625  -0.44481
 -0.44481    0.895625
```
```
julia> orth([1 8 12; 5 0 7 ; 6 4 1])
3×3 Array{Float64,2}:
 -0.856421   0.468442   0.217036
 -0.439069  -0.439714  -0.783498
 -0.27159   -0.766298   0.582259
```
"""
function orth(M::Matrix)
  matrixRank = rank(M)
  U, S, V = svd(M)
  return U[:,1:matrixRank]
end

