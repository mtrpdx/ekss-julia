# ekss.jl
# An implementation of the Ensemble K-Subspaces Clustering Algorithm in Julia
# Based on the work done by John Lipor, David Hong, Yan Shuo Tan, and Laura Balzano
#
# Martin Rodriguez 2019
# Portland State University

#=
Inputs:
    X     : data vector
    Nk    : number of candidate subspaces
    d     : candidate dimension
    K     : number of output clusters
    (below not implemented yet)
    q     : threshold parameter
    B     : number of base clusterings
    T     : number of KSS iterations

Output:
    C     : vector with clusters of data vector X
=#

#if abspath(PROGRAM_FILE) == @__FILE__
#    println("Current file ", PROGRAM_FILE, " being run as main script.\n")
#    using ArgParse
#end 

using ArgParse
using LinearAlgebra
using Gadfly    # Use Gadfly's spy() to visualize matrices


include("orth.jl")
include("SpectralClustering.jl")
include("missRate.jl")
include("myBestMap.jl")

function parseCommandline()
    s = ArgParseSettings()

    @add_arg_table s begin
        "--K"
            help = "Number of output clusters"
            arg_type = Int
            default = 7
        "--Nk"
            help = "Number of candidate subspaces"
            arg_type = Int
            default = 100
        "--D"
            help = "Ambient dimension"
            arg_type = Int
            default = 100
        "--d"
            help = "Candidate dimension"
            arg_type = Int
            default = 9
    end
    return parse_args(s)
end

parsed_args = parseCommandline()

# Generate data parameters from args
for (arg,val) in parsed_args
    @eval const $(Symbol(arg)) = $(val)
end

println("Generating data with  
    K (num. output clusters) = $(K)
    Nk (num. candidate subspaces) = $(Nk)
    D (ambient dim.) = $(D)
    d (candidate dimension) = $(d)...\n")
N = K*Nk
varn = 0.08        

# Generate data
X = reshape([],Nk,0)
trueLabels = reshape([],0,1)

for kk = 1:K
    global U = orth(randn(D,d))
    x = U*randn(d,Nk)
    global X = hcat(X,x)
    ks = kk*ones(Nk,1)
    global trueLabels = vcat(trueLabels, ks)
end
D, N = size(X)
X = X + sqrt(varn)*randn(size(X))
X = X./repeat(sqrt.(sum(X.^2,dims=1)),D,1)

# look at the Gram matrix - use this as affinity
G = abs.(X'*X)
estLabels = SpectralClustering(G,K)
err = missRate(trueLabels,estLabels)
println("err = ", err[1])
